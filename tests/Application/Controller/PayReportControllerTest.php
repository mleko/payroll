<?php
declare(strict_types=1);

namespace App\Tests\Application\Controller;

use App\Application\Controller\PayReportController;
use App\Domain\PayReport\ReportGenerator;
use App\Domain\PayReport\ValueObject\PayReportCriteria;
use App\Domain\PayReport\ValueObject\Report;
use App\Domain\PayReport\ValueObject\ReportRow;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class PayReportControllerTest extends TestCase
{

    private PayReportController $controller;
    /** @var ReportGenerator|MockObject */
    private ReportGenerator $generator;

    public function testGenerateWithCriteria() {
        $request = new Request();
        $request->query->set("department", "IT");
        $request->query->set("sort", "baseSalary");
        $request->query->set("sortDir", "desc");

        $this->generator->expects($this->once())->method("generateReport")
            ->with(
                $this->equalTo(new PayReportCriteria("IT", null, null, "baseSalary", false)),
                $this->isInstanceOf(\DateTimeInterface::class)
            )
            ->willReturn(new Report([]));

        $this->controller->generate($request);
    }

    public function testGenerateWithEmptyCriteria() {
        $request = new Request();

        $this->generator->expects($this->once())->method("generateReport")
            ->with(
                $this->equalTo(new PayReportCriteria(null, null, null, null, true)),
                $this->isInstanceOf(\DateTimeInterface::class)
            )
            ->willReturn(new Report([]));

        $this->controller->generate($request);
    }

    public function testGenerateWillNormalizeReport() {
        $request = new Request();

        $this->generator->method("generateReport")->willReturn(new Report([new ReportRow(
            "firstName", "lastName", "IT", 1000, 200, "flat", 1200
        )]));
        $response = $this->controller->generate($request);

        $this->assertEquals(new JsonResponse(
            ["rows" => [["firstName" => "firstName", "lastName" => "lastName",
                "department" => "IT", "baseSalary" => 1000, "bonus" => 200, "bonusType" => "flat", "totalSalary" => 1200
            ]]]
        ), $response);
    }

    protected function setUp(): void {
        parent::setUp();
        $this->generator = $this->createMock(ReportGenerator::class);
        $this->controller = new PayReportController($this->generator,
            new Serializer([new ObjectNormalizer()], []));
    }
}
