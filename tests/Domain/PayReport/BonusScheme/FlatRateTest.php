<?php
declare(strict_types=1);

namespace App\Tests\Domain\PayReport\BonusScheme;

use App\Domain\PayReport\BonusScheme\FlatRate;
use App\Entity\Department;
use App\Entity\Employee;
use PHPUnit\Framework\TestCase;

class FlatRateTest extends TestCase
{
    private FlatRate $calculator;

    /**
     * @dataProvider caseProvider
     */
    public function testCalculateBonus($hireDate, $rate, $date, $expected) {
        $department = new Department('', 'FLAT', 0);
        $employee = new Employee("", "", 0, $department, $hireDate);
        $actual = $this->calculator->calculateBonus($employee, $rate, $date);
        $this->assertEquals($expected, $actual);
    }

    public function caseProvider(): array {

        return [
            [new \DateTimeImmutable('2010-01-01 00:00'), 100, new \DateTimeImmutable('2010-03-01 00:00'), 0],
            [new \DateTimeImmutable('2010-01-01 00:00'), 100, new \DateTimeImmutable('2011-01-01 00:00'), 100],
            [new \DateTimeImmutable('2010-01-01 00:00'), 100, new \DateTimeImmutable('2015-01-01 00:00'), 500],
            [new \DateTimeImmutable('2010-01-01 00:00'), 100, new \DateTimeImmutable('2031-01-01 00:00'), 1000],
            [new \DateTimeImmutable('2010-01-01 00:00'), 42, new \DateTimeImmutable('2031-01-01 00:00'), 420],
        ];
    }

    protected function setUp(): void {
        parent::setUp();
        $this->calculator = new FlatRate();
    }
}
