<?php
declare(strict_types=1);

namespace App\Tests\Domain\PayReport\BonusScheme;

use App\Domain\PayReport\BonusScheme\Percent;
use App\Entity\Department;
use App\Entity\Employee;
use PHPUnit\Framework\TestCase;

class PercentTest extends TestCase
{
    private Percent $calculator;

    /**
     * @dataProvider caseProvider
     */
    public function testCalculateBonus($baseSalary, $rate, $expected) {
        $now = new \DateTimeImmutable();
        $department = new Department('', 'FLAT', 0);
        $employee = new Employee("", "", $baseSalary, $department, $now);
        $actual = $this->calculator->calculateBonus($employee, $rate, $now);
        $this->assertEquals($expected, $actual);
    }

    public function caseProvider(): array {

        return [
            [1000, 10, 100],
            [10000, 10, 1000],
            [4200, 50, 2100],
            [1000, 0, 0],
        ];
    }

    protected function setUp(): void {
        parent::setUp();
        $this->calculator = new Percent();
    }
}
