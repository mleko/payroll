<?php
declare(strict_types=1);

namespace App\Tests\Domain\PayReport;

use App\Domain\PayReport\BonusCalculator;
use App\Domain\PayReport\ReportGenerator;
use App\Domain\PayReport\Repository\EmployeeRepository;
use App\Domain\PayReport\ValueObject\PayReportCriteria;
use App\Domain\PayReport\ValueObject\Report;
use App\Domain\PayReport\ValueObject\ReportRow;
use App\Entity\Department;
use App\Entity\Employee;
use PHPUnit\Framework\TestCase;

class ReportGeneratorTest extends TestCase
{
    public function testReportGeneration() {
        $repository = $this->createMock(EmployeeRepository::class);
        $calculator = $this->createMock(BonusCalculator::class);

        $generator = new ReportGenerator($repository, $calculator);
        $now = new \DateTimeImmutable();
        $repository->method('findByReportCriteria')->willReturn([
            new Employee("name", "1", 1000, new Department("department", "NIL", 0), $now),
        ]);
        $calculator->method('calculateBonus')->willReturn(42.0);

        $payReportCriteria = new PayReportCriteria(null, null, null, null, true);
        $report = $generator->generateReport($payReportCriteria, $now);

        $this->assertEquals(new Report([
            new ReportRow("name", "1", "department", 1000, 42, "NIL", 1042),
        ]), $report);
    }

    public function testReportGenerationSort() {
        $repository = $this->createMock(EmployeeRepository::class);
        $calculator = $this->createMock(BonusCalculator::class);

        $generator = new ReportGenerator($repository, $calculator);
        $now = new \DateTimeImmutable();
        $repository->method('findByReportCriteria')->willReturn([
            new Employee("name", "1", 3000, new Department("department", "NIL", 0), $now),
            new Employee("name", "2", 1000, new Department("department", "NIL", 0), $now),
            new Employee("name", "3", 2000, new Department("department", "NIL", 0), $now),
        ]);
        $calculator->method('calculateBonus')->willReturn(42.0);

        $payReportCriteria = new PayReportCriteria(null, null, null, "totalSalary", true);
        $report = $generator->generateReport($payReportCriteria, $now);

        $this->assertEquals(new Report([
            new ReportRow("name", "2", "department", 1000, 42, "NIL", 1042),
            new ReportRow("name", "3", "department", 2000, 42, "NIL", 2042),
            new ReportRow("name", "1", "department", 3000, 42, "NIL", 3042),
        ]), $report);
    }
}
