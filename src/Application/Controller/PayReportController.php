<?php
declare(strict_types=1);

namespace App\Application\Controller;


use App\Domain\PayReport\ReportGenerator;
use App\Domain\PayReport\ValueObject\PayReportCriteria;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

class PayReportController
{
    private ReportGenerator $generator;
    private SerializerInterface $serializer;

    /**
     * PayReportController constructor.
     * @param ReportGenerator $generator
     * @param SerializerInterface $serializer
     */
    public function __construct(ReportGenerator $generator, SerializerInterface $serializer) {
        $this->generator = $generator;
        $this->serializer = $serializer;
    }

    #[Route('/report', name: 'payreport')]
    public function generate(Request $request): Response {
        $query = $request->query;
        $criteria = new PayReportCriteria(
            $query->get('department', null),
            $query->get('firstName', null),
            $query->get('lastName', null),
            $query->get('sort', null),
            $query->get('sortDir', null) !== 'desc',
        );

        $report = $this->generator->generateReport($criteria, new \DateTimeImmutable());
        return new JsonResponse($this->serializer->normalize($report));
    }
}
