<?php

namespace App\Entity;

use App\Infrastructure\Doctrine\Repository\DepartmentRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: DepartmentRepository::class)]
class Department
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private ?int $id;

    #[ORM\Column(type: 'string', length: 255)]
    private string $name;

    #[ORM\Column(type: 'string', length: 32)]
    private string $bonusScheme;

    #[ORM\Column(type: 'float')]
    private float $bonusRate;

    /**
     * Department constructor.
     * @param string $name
     * @param string $bonusScheme
     * @param float $bonusRate
     * @param int|null $id
     */
    public function __construct(string $name, string $bonusScheme, float $bonusRate, ?int $id = null) {
        $this->name = $name;
        $this->bonusScheme = $bonusScheme;
        $this->bonusRate = $bonusRate;
        $this->id = $id;
    }

    public function getId(): ?int {
        return $this->id;
    }

    public function getName(): string {
        return $this->name;
    }

    public function setName(string $name): self {
        $this->name = $name;

        return $this;
    }

    public function getBonusScheme(): string {
        return $this->bonusScheme;
    }

    public function setBonusScheme(string $bonusScheme): self {
        $this->bonusScheme = $bonusScheme;

        return $this;
    }

    public function getBonusRate(): float {
        return $this->bonusRate;
    }

    public function setBonusRate(string $bonusRate): self {
        $this->bonusRate = $bonusRate;

        return $this;
    }
}
