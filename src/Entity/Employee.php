<?php

namespace App\Entity;

use App\Infrastructure\Doctrine\Repository\EmployeeRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: EmployeeRepository::class)]
class Employee
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private ?int $id;

    #[ORM\Column(type: 'string', length: 255)]
    private string $firstName;

    #[ORM\Column(type: 'string', length: 255)]
    private string $lastName;

    #[ORM\Column(type: 'float')]
    private float $baseSalary;

    #[ORM\ManyToOne(targetEntity: Department::class)]
    #[ORM\JoinColumn(nullable: false)]
    private Department $department;

    #[ORM\Column(type: 'datetime_immutable')]
    private \DateTimeImmutable $hireDate;

    /**
     * Employee constructor.
     * @param string $firstName
     * @param string $lastName
     * @param float $baseSalary
     * @param Department $department
     * @param \DateTimeImmutable $hireDate
     * @param int|null $id
     */
    public function __construct(string $firstName, string $lastName, float $baseSalary, Department $department, \DateTimeImmutable $hireDate, ?int $id = null) {
        $this->firstName = $firstName;
        $this->lastName = $lastName;
        $this->baseSalary = $baseSalary;
        $this->department = $department;
        $this->hireDate = $hireDate;
        $this->id = $id;
    }

    public function getId(): ?int {
        return $this->id;
    }

    public function getFirstName(): string {
        return $this->firstName;
    }

    public function setFirstName(string $firstName): self {
        $this->firstName = $firstName;

        return $this;
    }

    public function getLastName(): string {
        return $this->lastName;
    }

    public function setLastName(string $lastName): self {
        $this->lastName = $lastName;

        return $this;
    }

    public function getBaseSalary(): float {
        return $this->baseSalary;
    }

    public function setBaseSalary(float $baseSalary): self {
        $this->baseSalary = $baseSalary;

        return $this;
    }

    public function getDepartment(): Department {
        return $this->department;
    }

    public function setDepartment(Department $department): self {
        $this->department = $department;

        return $this;
    }

    public function getHireDate(): \DateTimeImmutable {
        return $this->hireDate;
    }

    public function setHireDate(\DateTimeImmutable $hireDate): self {
        $this->hireDate = $hireDate;

        return $this;
    }
}
