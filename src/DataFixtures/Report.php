<?php

namespace App\DataFixtures;

use App\Entity\Department;
use App\Entity\Employee;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class Report extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $itDepartment = new Department("IT", "FLAT", 10);
        $manager->persist($itDepartment);
        $hrDepartment = new Department("HR", "PERCENT", 10);
        $manager->persist($hrDepartment);

        $itEmp1 = new Employee("IT1", "IT1", 1300, $itDepartment, new \DateTimeImmutable("2000-01-01 00:00:00"));
        $manager->persist($itEmp1);
        $itEmp2 = new Employee("IT2", "IT2", 1000, $itDepartment, new \DateTimeImmutable("2005-01-01 00:00:00"));
        $manager->persist($itEmp2);
        $hrEmp1 = new Employee("HR1", "HR1", 1300, $hrDepartment, new \DateTimeImmutable("2000-01-01 00:00:00"));
        $manager->persist($hrEmp1);
        $hrEmp2 = new Employee("HR2", "HR2", 1000, $hrDepartment, new \DateTimeImmutable("2005-01-01 00:00:00"));
        $manager->persist($hrEmp2);

        $manager->flush();
    }
}
