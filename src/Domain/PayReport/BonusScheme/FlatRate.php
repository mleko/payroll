<?php
declare(strict_types=1);

namespace App\Domain\PayReport\BonusScheme;


use App\Entity\Employee;

class FlatRate implements BonusScheme
{

    const NAME = "FLAT";

    public function calculateBonus(Employee $employee, float $rate, \DateTimeInterface $date) {
        $period = $date->diff($employee->getHireDate());
        $years = $period->y <= 10 ? $period->y : 10;
        return $rate * $years;
    }

    public function getName(): string {
        return self::NAME;
    }
}
