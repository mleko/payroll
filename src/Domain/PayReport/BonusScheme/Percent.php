<?php
declare(strict_types=1);

namespace App\Domain\PayReport\BonusScheme;


use App\Entity\Employee;

class Percent implements BonusScheme
{
    const NAME = "PERCENT";

    public function calculateBonus(Employee $employee, float $rate, \DateTimeInterface $date) {
        return $employee->getBaseSalary() * $rate / 100.0;
    }

    public function getName(): string {
        return self::NAME;
    }
}
