<?php
declare(strict_types=1);

namespace App\Domain\PayReport\BonusScheme;


use App\Entity\Employee;

interface BonusScheme
{
    public function calculateBonus(Employee $employee, float $rate, \DateTimeInterface $date);

    public function getName(): string;
}
