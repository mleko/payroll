<?php
declare(strict_types=1);

namespace App\Domain\PayReport;


use App\Domain\PayReport\Repository\EmployeeRepository;
use App\Domain\PayReport\ValueObject\PayReportCriteria;
use App\Domain\PayReport\ValueObject\Report;
use App\Domain\PayReport\ValueObject\ReportRow;
use App\Entity\Employee;

class ReportGenerator
{
    private EmployeeRepository $repository;
    private BonusCalculator $bonusCalculator;

    /**
     * ReportGeneratorImpl constructor.
     * @param EmployeeRepository $repository
     * @param BonusCalculator $bonusCalculator
     */
    public function __construct(EmployeeRepository $repository, BonusCalculator $bonusCalculator) {
        $this->repository = $repository;
        $this->bonusCalculator = $bonusCalculator;
    }

    public function generateReport(PayReportCriteria $criteria, \DateTimeInterface $date): Report {
        $users = $this->repository->findByReportCriteria($criteria);
        $data = $this->processEmployees($users, $date);

        if ($criteria->getSortColumn()) {
            $field = $criteria->getSortColumn();
            $asc = $criteria->isSortAsc();
            \usort($data, function ($a, $b) use ($field, $asc) {
                return $asc ? ($a[$field] <=> $b[$field]) : -($a[$field] <=> $b[$field]);
            });
        }
        $rows = [];
        foreach ($data as $datum) {
            $rows[] = new ReportRow(
                $datum['firstName'],
                $datum['lastName'],
                $datum['department'],
                $datum['baseSalary'],
                $datum['bonus'],
                $datum['bonusType'],
                $datum['totalSalary'],
            );
        }
        return new Report($rows);
    }

    private function processEmployees(array $employees, \DateTimeInterface $date) {
        $data = [];
        foreach ($employees as $employee) {
            $data[] = $this->processEmployee($employee, $date);
        }
        return $data;
    }

    private function processEmployee(Employee $employee, \DateTimeInterface $date): array {
        $bonus = $this->bonusCalculator->calculateBonus($employee, $date);
        $department = $employee->getDepartment();
        return [
            'firstName' => $employee->getFirstName(),
            'lastName' => $employee->getLastName(),
            'department' => $department->getName(),
            'baseSalary' => $employee->getBaseSalary(),
            'bonus' => $bonus,
            'bonusType' => $department->getBonusScheme(),
            'totalSalary' => $employee->getBaseSalary() + $bonus
        ];
    }
}
