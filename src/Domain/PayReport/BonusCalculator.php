<?php
declare(strict_types=1);

namespace App\Domain\PayReport;


use App\Domain\PayReport\BonusScheme\BonusScheme;
use App\Entity\Employee;

class BonusCalculator
{

    /**
     * @var BonusScheme[]
     */
    private array $map = [];

    /**
     * BonusCalculator constructor.
     * @param BonusScheme[] $schemes
     */
    public function __construct(array $schemes) {
        foreach ($schemes as $scheme) {
            $this->addBonusScheme($scheme);
        }
    }

    public function calculateBonus(Employee $employee, \DateTimeInterface $date): float {
        $department = $employee->getDepartment();
        if (!\array_key_exists($department->getBonusScheme(), $this->map)) {
            throw new \RuntimeException("Unsupported bonus scheme");
        }
        $scheme = $this->map[$department->getBonusScheme()];
        return $scheme->calculateBonus($employee, $department->getBonusRate(), $date);
    }

    public function addBonusScheme(BonusScheme $scheme) {
        $this->map[$scheme->getName()] = $scheme;
    }
}
