<?php
declare(strict_types=1);

namespace App\Domain\PayReport\ValueObject;


class PayReportCriteria
{
    private ?string $departmentNameFilter;
    private ?string $firstNameFilter;
    private ?string $lastNameFilter;

    private ?string $sortColumn;
    private bool $sortAsc;

    /**
     * Criteria constructor.
     * @param string|null $departmentNameFilter
     * @param string|null $firstNameFilter
     * @param string|null $lastNameFilter
     * @param string|null $sortColumn
     * @param bool $sortAsc
     */
    public function __construct(?string $departmentNameFilter, ?string $firstNameFilter, ?string $lastNameFilter, ?string $sortColumn, bool $sortAsc) {
        $this->departmentNameFilter = $departmentNameFilter;
        $this->firstNameFilter = $firstNameFilter;
        $this->lastNameFilter = $lastNameFilter;
        $this->sortColumn = $sortColumn;
        $this->sortAsc = $sortAsc;
    }

    /**
     * @return string|null
     */
    public function getDepartmentNameFilter(): ?string {
        return $this->departmentNameFilter;
    }

    /**
     * @return string|null
     */
    public function getFirstNameFilter(): ?string {
        return $this->firstNameFilter;
    }

    /**
     * @return string|null
     */
    public function getLastNameFilter(): ?string {
        return $this->lastNameFilter;
    }

    /**
     * @return string|null
     */
    public function getSortColumn(): ?string {
        return $this->sortColumn;
    }

    /**
     * @return bool
     */
    public function isSortAsc(): bool {
        return $this->sortAsc;
    }
}
