<?php
declare(strict_types=1);

namespace App\Domain\PayReport\ValueObject;


class ReportRow
{
    private string $firstName;
    private string $lastName;
    private string $department;
    // Money type would be a better fit
    private float $baseSalary;
    private float $bonus;
    private string $bonusType;
    private float $totalSalary;

    /**
     * ReportRow constructor.
     * @param string $firstName
     * @param string $lastName
     * @param string $department
     * @param float $baseSalary
     * @param float $bonus
     * @param string $bonusType
     * @param float $totalSalary
     */
    public function __construct(string $firstName, string $lastName, string $department, float $baseSalary, float $bonus, string $bonusType, float $totalSalary) {
        $this->firstName = $firstName;
        $this->lastName = $lastName;
        $this->department = $department;
        $this->baseSalary = $baseSalary;
        $this->bonus = $bonus;
        $this->bonusType = $bonusType;
        $this->totalSalary = $totalSalary;
    }

    /**
     * @return string
     */
    public function getFirstName(): string {
        return $this->firstName;
    }

    /**
     * @return string
     */
    public function getLastName(): string {
        return $this->lastName;
    }

    /**
     * @return string
     */
    public function getDepartment(): string {
        return $this->department;
    }

    /**
     * @return float
     */
    public function getBaseSalary(): float {
        return $this->baseSalary;
    }

    /**
     * @return float
     */
    public function getBonus(): float {
        return $this->bonus;
    }

    /**
     * @return string
     */
    public function getBonusType(): string {
        return $this->bonusType;
    }

    /**
     * @return float
     */
    public function getTotalSalary(): float {
        return $this->totalSalary;
    }
}
