<?php
declare(strict_types=1);

namespace App\Domain\PayReport\ValueObject;


class Report
{
    /** @var ReportRow[] */
    private array $rows;

    /**
     * Report constructor.
     * @param ReportRow[]|array $rows
     */
    public function __construct($rows) {
        $this->rows = $rows;
    }

    /**
     * @return ReportRow[]
     */
    public function getRows(): array {
        return $this->rows;
    }
}
