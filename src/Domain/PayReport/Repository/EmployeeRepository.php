<?php
declare(strict_types=1);

namespace App\Domain\PayReport\Repository;


use App\Domain\PayReport\ValueObject\PayReportCriteria;
use App\Entity\Employee;

interface EmployeeRepository
{
    /**
     * @param PayReportCriteria $criteria
     * @return Employee[]
     */
    public function findByReportCriteria(PayReportCriteria $criteria): array;
}
